var request = require('request');
var cheerio = require('cheerio');
var Promise = require('bluebird');
var fs = require('fs');


var scrapCategory = function(){
	return new Promise(function(resolve, reject){
		request('https://m.bnizona.com/index.php/category/index/promo', function (error, response, html) {
		  if (!error && response.statusCode == 200) {
		    var $ = cheerio.load(html);
		    var result = [];
		    $('ul.menu>li>a').each(function(i, element){
		      	var a = $(this);
		      	var stash = {title: a.text(),
		      				url: a.attr('href')
		      			}
		     	result.push(stash);
		    });
		    resolve(result);
		  }
		  else{
		  	reject(error);
		  }
		});
	});
}

var requestPromo = function(category) {
	return new Promise(function(resolve, reject){
		request(category.url, function (error, response, html) {
		  if (!error && response.statusCode == 200) {
		    var $ = cheerio.load(html);
		    var result = [];
		    var obj = {};
		    $('ul.list2>li>a').each(function(i, element){
		      	var a = $(this);
		      	var url = a.attr('href');
		      	var stash = {	'url': url,
		      					'img-url': a.find('img').attr('src'), 
		      					'merchant-name': a.find('.merchant-name').text(),
		      					'promo-title': a.find('.promo-title').text(),
		      					'valid-until': a.find('.valid-until').text(),
		      					'details' : {}
		      				};
		      	result.push(stash);
		    });
		    obj[category.title] = result;
		    resolve(obj);
		  }
		  else{
		  	reject(error);
		  }
		});
	});
}


var requestDetails = function(category, data, index) {
	return new Promise(function(resolve, reject){
		var key = Object.keys(category)
		listDetails = category[key];
		Promise.all(listDetails.map(function(list, i){
			return new Promise(function(resolve,reject){
				request(list.url, function (error, response, html) {
				  if (!error && response.statusCode == 200) {
				    var $ = cheerio.load(html);
				    var a = $('ul.menu>li').children();
				    
				    var result = {	
				    	'banner-url': $('.banner>img').attr('src'),
				    	'title' : a.find('h5').text(),
				      	'terms-condition': a.find('p').text(),
				      	'merchant-location': $('#merchant-location>div>p').text()
				      	};
					data[index][key][i].details = result;
				  	resolve(i);
				  }
				  else{
				  	reject(error);
				  }
				});
			});

		})).then(function(result){
			resolve(result);
		});
	});
}

var scrapDetails = function(data){
	return new Promise(function(resolve, reject){
		var promises = [];
		for(i=0; i<data.length;i++){
			var category = data[i];
			promises.push(requestDetails(category, data, i));
		}
		Promise.all(promises).then(function(result){
			console.log('Complete scrapping url');
			resolve(data);
		})
	});
}
var scrapContent = function(category){
	return new Promise(function(resolve, reject){
		var promises = [];
		category.forEach(function(category){
			promises.push(requestPromo(category));
		});
		Promise.all(promises).then(function(data){
			resolve(data);
		});
	});
}

var saveFile = function(data){
	return new Promise(function(resolve, reject){
		console.log('writing');
		fs.writeFile('solution.json', JSON.stringify(data,null,4) , function (err) {
	  		if (err) reject(err);
	  		console.log('Saved to > solution.json');
		});
	});
}

scrapCategory().then(function(result){
	return scrapContent(result).then(function(result){
		return scrapDetails(result).then(function(result){
			return saveFile(result);
		});
	});
})